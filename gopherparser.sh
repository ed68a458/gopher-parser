#!/bin/bash
#
# Example Bash CGI script.
#
# Caveat: Remember the empty line after echoing headers
#

# httputils creates the associative arrays POST_PARAMS and GET_PARAMS
if [[ "$SCRIPT_FILENAME" ]]; then
    . "$(dirname $SCRIPT_FILENAME)/httputils"
else
    . "$(dirname $(pwd)$SCRIPT_NAME)/httputils"
fi

# Common headers goes here
echo "Content-Type: text/html; charset=UTF-8"
#echo "Status: 405 Method Not Allowed"
echo "Status: 200 OK"
echo "Content-Type: text/html; charset=UTF-8"
echo ""

url=$QUERY_STRING
if [ -n "$(echo $url|grep ^gopher)" ]
then
    site=$url
    base_url=$(echo $site|grep -o "gopher.*\/[0,1]\/"|sed s,/[0,1]/,,) 
    s=$(echo $site|grep -o "gopher.*\/[0,1]\/"|sed s,$base_url,,|cut -c2-2)
    if [ -z "$s" ] 
    then 
        s=1
        base_url=$(echo $site|sed s,/$,,)
    fi
    if [ ${s:0:1} == "1" ]
    then
        flag=1
    else
        flag=0
    fi
else
    site=gopher://localhost:70/$url
    
    if [ ${url:0:1} == "1" ]
    then
        flag=1
    else
        flag=0
    fi
fi
cat ./ed-css.txt
if [ -n "$(echo $site|grep localhost)" ]
then
    if [ $flag == "1" ]
    then
    echo "<pre>" 
        curl $site |awk -F"\t" '{
            if (/^0/) print "<a href=http://ed68a458.xyz:80/cgi-bin/gopherparser.sh?0"$2">"substr($1,2)"</a>"
            else if (/^1/) print "<a href=http://ed68a458.xyz:80/cgi-bin/gopherparser.sh?1"$2">"substr($1,2)"</a>"
            else if (/^h/) print "<a href="substr($2,5)">"substr($1,2)"</a>"
            else print substr($1,2)""  
        }'
    echo "</pre>" 
    else 
    echo "<pre>" 
        curl $site|sed -E  "s,((http|https|ftp|gopher)|mailto):(//)?[^ <>\"\t]*|(www|ftp)[0-9]?.?*,<a href=\"&\">&</a>,g"
        #curl gopher://localhost:70/$url|sed s/$/"<br>"/ 
    echo "</pre>" 
    fi
else
    if [ $flag == "1" ]
    then
    echo "<pre>" 
        curl $site |awk -v var=$base_url -F"\t" '{
            if (/^0/) print "<a href=http://ed68a458.xyz:80/cgi-bin/gopherparser.sh?"var"/0"$2">"substr($1,2)"</a>"
            else if (/^1/) print "<a href=http://ed68a458.xyz:80/cgi-bin/gopherparser.sh?"var"/1"$2">"substr($1,2)"</a>"
            else if (/^h/) print "<a href="substr($2,5)">"substr($1,2)"</a>"
            else print substr($1,2)""  
        }'
    echo "</pre>" 
    else 
    echo "<pre>" 
        curl $site
    echo "</pre>" 
    fi

fi

echo "<p>Follow me on <a href=https://gab.com/ed68a458_1>Gab</a></p>"  
echo "</body></head>"  
