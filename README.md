# ed68a458's Gopher Parser

Gopher is an alternative protocol to http. It was actually invented before
http. It's simpler, it doesn't have javascript and therefore can't track users.
Text files exchanged with this method have the following advantages:

- they are small: even the slowest connections can exchange simple text files
- they have no malware by design: the text cannot execute code (it is also
  possible to exchange programs, but it is not efficient with this protocol)
- they do not track the user: the server holder can keep the logs, but cannot
  distribute the data with advertising managers e.g. AdSense
- no cookies: it is not possible to insert cookies on the pages because they
  cannot execute code

Despite all these advantages, gopher is little used, mainly because people are
not used to a text-only screen and because modern browsers have stopped
supporting this protocol.

For this reason I have created this gopher parser.

## Use

Using it is easy: just put the gopher site after the question mark. Recall to
put the `gopher://` before the site. Example:

```
http://ed68a458.xyz/cgi-bin/gopherparser.sh?gopher://bitreich.org
```

## Example
